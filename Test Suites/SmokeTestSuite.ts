<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SmokeTestSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f9f53952-e70d-4b6a-a67b-2025307e6fdf</testSuiteGuid>
   <testCaseLink>
      <guid>caeedb09-e137-472f-a554-b8f0338553ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/InvalidLoginTestCase</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9eae21a-6d48-4107-8b00-8292b67b17ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ValidLoginTestCase</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d8448a92-84fb-411b-8d8d-35f52c3b3ca6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MakingAnAppointmentTestCase</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
